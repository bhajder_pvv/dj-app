import moment from 'moment';
import uuid from 'uuid';

// export default values => {
//     const errors = {};
//     if(!values.name){
//         errors.name = "Required!"
//     }
//     if (!values.email) {
//         errors.email = 'Required'
//     } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
//         errors.email = 'Type valid email address'
//     }
//     if(!values.message){
//         errors.message = "Required!"
//     } else if (values.message.length > 300) {
//         errors.message = "Max 300 characters"
//     }
//     return errors;
// };

export const required = value => (
    value ? undefined : "Required!"
)

export const email = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Type valid email address'
        : undefined;

export const maxLength = max => value =>
    value && value.length > max
        ? `Max ${max} characters`
        : undefined;

export const date = value =>
    value && moment(value, 'MM/DD/YYYY', true).isValid()
        ? undefined
        : 'Choose correct date format - MM/DD/YYYY';

export const id = value =>
    console.log(value);
