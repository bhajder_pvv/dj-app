const defaultCompositionsState = [{
        id: 'rCqQGgM0Y5I',
        title: 'Sam Feldt ft. Kimberly Anne - Show Me Love (EDX\'s Indian Summer Remix)',
        description: '"Radioactive" is a song recorded by American rock band Imagine Dragons for their major-label debut EP Continued Silence and later on their debut studio album, Night Visions (2012), as the opening track. "Radioactive" was first sent to modern rock radio on October 29, 2012,[1] and released to contemporary radio on April 9, 2013. Musically, "Radioactive" is an alternative rock song with elements of electronic rock and contains cryptic lyrics about apocalyptic and revolutionist themes.',
        date: '03.04.2015',
        channelTitle: 'indian sammer',
        viewCount: 1824672,
        likeCount: 10042,
        commentCount: 96
}];

export const getItems = (key) => {
        try {
                const item = localStorage.getItem(key);
                if(item === null) {
                        switch(key) {
                                case 'compositions': return defaultCompositionsState;
                                case 'contacts' : return [];
                                default: return []
                        }
                }
                return JSON.parse(item);

        } catch (err) {
                return [];
        }
};

export const setItems = (key, value) => {
        localStorage.setItem(key, value);
};