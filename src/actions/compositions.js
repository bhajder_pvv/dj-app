//ADD__COMPOSITION
export const addComposition = (composition) => ({
    type: 'ADD_COMPOSITION',
    composition
});