//ADD_CONTACT
export const addContact = (contact) => ({
    type: 'ADD_CONTACT',
    contact
});

//EDIT_CONTACT
export const editContact = (id, updates) => ({
    type: 'EDIT_CONTACT',
    id,
    updates
});