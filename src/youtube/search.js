import YTSearch from "youtube-search";
const KEY = 'AIzaSyCme6WOdBsYj2JXjNJxvmv_SgPIc2-z79E';
const KEY_TEST = 'AIzaSyBuTxpEbRSSs8mcMAwWeaS4-RQVtNt8SWM';
const KEY_BACKUP = 'AIzaSyCEmmQBQ9HQ9QzyHeTsS0c5HNS0H-SH8S4';
const KEY_FINAL = 'AIzaSyBimjw8Wvp1PDfFtX--VqZjG6oD5myMFY4';

export default(term) => {

        return new Promise((resolve, reject) => YTSearch(term, {key: KEY_TEST, maxResults: 6, part: 'snippet'}, (err, videos) => {
            if(err){
               reject(err);
            } else {
                resolve(videos.map(video => video.id));
            }
        }));
};