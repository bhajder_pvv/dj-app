import axios from 'axios';
const KEY = 'AIzaSyCme6WOdBsYj2JXjNJxvmv_SgPIc2-z79E';
const KEY_TEST = 'AIzaSyBuTxpEbRSSs8mcMAwWeaS4-RQVtNt8SWM';
const KEY_BACKUP = 'AIzaSyCEmmQBQ9HQ9QzyHeTsS0c5HNS0H-SH8S4';
const KEY_FINAL = 'AIzaSyBimjw8Wvp1PDfFtX--VqZjG6oD5myMFY4';

export default (video) => {
         return axios.create({
            baseURL: 'https://www.googleapis.com/youtube/v3',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json'
            },
            withCredentials: true,
            credentials: 'same-origin'
        }).get("/videos?part=snippet%2C+statistics&id=" + video + "&maxResults=5&key=" + KEY_TEST);
}