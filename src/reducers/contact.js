import { getItems } from '../localstorage/localstorage';
// Contact Listing reducer

const persistedState = getItems("contacts");
export default (state = persistedState, action) => {
    switch (action.type) {
        case 'ADD_CONTACT':
            return [...state, action.contact];
        case 'EDIT_CONTACT':
            return state.map(contact => {
                if (contact.id === action.id) {
                    return {
                        ...contact,
                        ...action.updates
                    };
                } else {
                    return contact;
                };
            });
        default:
            return state;
    }
};