import { getItems } from '../localstorage/localstorage';
// Compositions Reducer

//
// const setDefaultState = () => {
//     return getItems("compositions");
// };
const persistedState = getItems("compositions");
export default (state = persistedState, action) => {
    switch (action.type) {
        case 'ADD_COMPOSITION':
            return [...state, action.composition];
        default:
            return state;
    }
};