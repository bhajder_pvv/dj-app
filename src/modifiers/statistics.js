export default (number) => {
    let output;
    if(number >= 1000 & number <1000000) {
        output = String(Math.round(number/1000) + 'K');
        return output;
    } else if (number >= 1000000 & number < 1000000000) {
        output = String(Math.round((number/1000000) * 10)/10 + 'M');
        return output;
    } else if (number >= 1000000000) {
        output = String(Math.round((number/1000000000) * 10)/10 + 'B');
        return output;
    }
    else return number;
};