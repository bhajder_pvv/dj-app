import { createStore, compose, combineReducers, applyMiddleware } from 'redux';
import compositionsReducer from '../reducers/compositions';
import contactReducer from '../reducers/contact'
import { reducer as formReducer } from 'redux-form';
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({compositions: compositionsReducer, contacts: contactReducer, form: formReducer})

export default () => {
    const store = createStore(
        rootReducer,
        composeEnhancers(applyMiddleware(thunk))
    );
    return store;
}