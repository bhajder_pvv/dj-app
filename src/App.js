import React, { useState } from 'react';
import { Provider } from 'react-redux';
import './App.css';
import { BreakpointProvider } from 'react-socks';
import AddCompositionButton from './components/AddCompositionModal/AddCompositionButton';
import ContactListingButton from './components/ContactListingModal/ContactListingButton';
import AboutSection from './components/AboutSection';
import DiscographySection from "./components/DiscographySection";
import ConcertToursSection from "./components/ConcertToursSection";
import LatterCompositionsSection from "./components/LatterCompositionsSection";
import TestimonialsSection from "./components/TestimonialsSection";
import NewsSection from "./components/NewsSection";
import UpcomingEventsSection from "./components/UpcomingEventsSection";
import HistorySection from "./components/HistorySection";
import ContactSection from "./components/ContactSection";
import configureStore from "./store/configureStore";
import AddCompositionModal from "./components/AddCompositionModal/AddCompositionModal";
import ContactListingModal from "./components/ContactListingModal/ContactListingModal";
import FansModal from "./components/FansModal/FansModal";
import FansButton from "./components/FansModal/FansButton";
import ReactCSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import {setItems} from "./localstorage/localstorage";

const store = configureStore();

store.subscribe(() => {
    setItems('compositions', JSON.stringify(store.getState().compositions));
    setItems('contacts', JSON.stringify(store.getState().contacts))
});

const App = () => {

    const [isAddCompositionModalOpen, changeIsAddCompositionModalOpen] = useState(false);
    const [isContactListingModalOpen, changeIsContactListingModalOpen] = useState(false);
    const [isFansModalOpen, changeIsFansModalOpen] = useState(false);

    const handleAddCompositionModal = () => {
        changeIsAddCompositionModalOpen(!isAddCompositionModalOpen);
    };
    const handleContactListingModal = () => {
        changeIsContactListingModalOpen(!isContactListingModalOpen);
    };
    const handleFansModal = () => {
        changeIsFansModalOpen(!isFansModalOpen);
    };

    return (
        <Provider store={store}>
            <BreakpointProvider>
            <ReactCSSTransitionGroup
                transitionName="fade"
                transitionAppear={true}
                transitionAppearTimeout={800}
                transitionEnterTimeout={800}
                transitionLeaveTimeout={800}
            >
                {(!isAddCompositionModalOpen && !isContactListingModalOpen && !isFansModalOpen) && <AddCompositionButton onButtonClick={handleAddCompositionModal}/>}
                {isAddCompositionModalOpen && <AddCompositionModal onClose={handleAddCompositionModal} isOpen={isAddCompositionModalOpen}/>}
                {(!isContactListingModalOpen && !isAddCompositionModalOpen && !isFansModalOpen) && <ContactListingButton onButtonClick={handleContactListingModal}/>}
                {isContactListingModalOpen && <ContactListingModal onClose={handleContactListingModal} isOpen={isContactListingModalOpen}/>}
                {(!isContactListingModalOpen && !isAddCompositionModalOpen && !isFansModalOpen) && <FansButton onButtonClick={handleFansModal}/>}
                {isFansModalOpen && <FansModal onClose={handleFansModal} isOpen={isFansModalOpen}/>}
            </ReactCSSTransitionGroup>
                <AboutSection/>
                <DiscographySection/>
                <ConcertToursSection/>
                <LatterCompositionsSection/>
                <TestimonialsSection/>
                <NewsSection/>
                <UpcomingEventsSection/>
                <HistorySection/>
                <ContactSection/>
            </BreakpointProvider>
        </Provider>
    );
};

export default(App);
