import React from 'react';
import styled, { keyframes } from 'styled-components';

const ring = keyframes`
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
`;

const Loader = styled.div`
   position: absolute;
   width: 100%;
   height: 100%;
   background: black;
   display: flex;
   justify-content: center;
   align-items: center;
   div {
     box-sizing: border-box;
    display: block;
    position: absolute;
    width: 51px;
    height: 51px;
    margin: 6px;
    border: 6px solid #fff;
    border-radius: 50%;
    animation: ${ring} 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
    border-color: #fff transparent transparent transparent;
    :nth-child(1) {
    animation-delay: -0.45s;
    }
    :nth-child(2) {
    animation-delay: -0.3s;
    }
    :nth-child(2) {
    animation-delay: -0.15s;
    }
   }
   
`;



const LoaderComponent = () => {
   return (
      <Loader>
         <div></div>
         <div></div>
         <div></div>
         <div></div>
      </Loader>
   );
}

export default LoaderComponent;
