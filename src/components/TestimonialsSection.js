import React from 'react';
import styled from 'styled-components';
import Slider from 'react-slick';
import note from '../Assets/images/note.png';
import bg from '../Assets/images/testimonials.jpg';
import arrow from '../Assets/images/arrow.png';

const Section = styled.section`
    height: 40em;
    overflow: hidden;
    background: url(${bg}) no-repeat center center;
    
     .slick-prev::before, .slick-next::before {
      content: "";
      background: url(${arrow}) no-repeat center center;
      background-size: 8px auto;
      width: 20px;
      height: 20px;
    }
`;

const Testimonial = styled.div`
    display: flex !important;
    justify-content: space-around;
    align-items: center;
    flex-direction: column;
    min-height: 400px;
    width: 300px !important;
    text-align: center;
    outline: none;
    
    img {
      object-fit: contain;
    }
    
    h2 {
      font-family: 'dosisregular';
      color: white;
      text-transform: uppercase;
    }
    
    p {
      color: #798285;
    }
    
    span {
      color: #565e61; 
    }
    
    @media(min-width: 640px) {
        width: 500px !important;
    }
    
    @media(min-width: 1024px) {
        width: 800px !important;
        padding: 2em 8em;
    }
`;

const testimonials = [{
        id: 1,
        title: 'In 2008, lead singer Dan Reynolds met drummer Andrew Tolman at Brigham Young University where they were both students',
        content: 'Tolman recruited longtime high school friend Daniel Wayne "Wing" Sermon, who had graduated from Berklee College of Music. ' +
            'Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys.',
        date: '- Early years (2008–10)'
    }, {
        id: 2,
        title: 'In 2008, lead singer Dan Reynolds met drummer Andrew Tolman at Brigham Young University where they were both students',
        content: 'Tolman recruited longtime high school friend Daniel Wayne "Wing" Sermon, who had graduated from Berklee College of Music. ' +
            'Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys.',
        date: '- Early years (2008–10)'
    }, {
        id: 3,
        title: 'In 2008, lead singer Dan Reynolds met drummer Andrew Tolman at Brigham Young University where they were both students',
        content: 'Tolman recruited longtime high school friend Daniel Wayne "Wing" Sermon, who had graduated from Berklee College of Music. ' +
            'Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys.',
        date: '- Early years (2008–10)'
    }
    ];

const TestimonialsSection = () => {

    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
        draggable: true
    };



    return (
        <Section>
            <Slider {...settings}>
                {testimonials.map(testimonial =>
                    <Testimonial key={testimonial.id}>
                        <img src={note} alt="note" />
                        <h2>{testimonial.title}</h2>
                        <p>{testimonial.content}</p>
                        <span>{testimonial.date}</span>
                    </Testimonial>
                )}
            </Slider>
        </Section>
    );
}

export default TestimonialsSection;