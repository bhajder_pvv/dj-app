import React from 'react';
import styled from 'styled-components';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUsers} from "@fortawesome/free-solid-svg-icons";

const Button = styled.button`
    bottom: 10em;
    z-index: 999;
    :hover {
      background: #000 !important;
    }
    @media(min-width: 1024px) {
      display: block;
    }
`;

const FansButton = (props) => {

    const handleParentChange = () => {
        props.onButtonClick();
    };

    return  (
        <Button onClick={handleParentChange} className="btn btn__icon">
            <FontAwesomeIcon icon={faUsers} />
        </Button>
    );
}

export default FansButton;
