import React, {useState, useEffect} from 'react';
import styled from 'styled-components';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import getFans from '../../requests/fansRequest';

const Listing = styled.div`
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
`;

const Fan = styled.div`
  position: relative;
  text-align: left;
  width: 30em;
  height: 10em;
  padding: 2em;
  background: #f8f8f8;
  margin: 2em;
  p {
  margin-top: 1em;
    color: #111;
  }
  img {
    position: absolute;
    right: 1em;
    top: 1em;
    height: 3em;
    border-radius: 50%;
  }
`;

const FansListing = () => {

const [fans, setFans] = useState();

useEffect(() => {
  getFans().then((fans) => {
      setFans(fans.data);
  });
},[]);

    return (
        <Listing>
            {fans
              ? (fans.length>=1) && fans.map(fan =>
                  <Fan key={fan._id}>
                    <h2>{fan.name.first}&nbsp;{fan.name.last}</h2>
                    <h3>{fan.email}</h3>
                    <p>Favorite song: {fan.favoriteFruit}</p>
                    <img src={fan.picture} alt="fan_picture"/>
                  </Fan>
               ) 
              : "Loading"
            }
        </Listing>
    )
}

export default FansListing;