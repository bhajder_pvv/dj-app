import React, { useState } from 'react';
import Modal from 'react-modal';
import styled from "styled-components";
import FansListing from './FansListing';

const Close = styled.div`
    position: absolute;
    right: 2em;
    :hover {
      cursor: pointer;
    }
    ::before, ::after {
      content: "";
      background: #000;
    }
`;

const Back = styled.div`
  position: absolute;
  left: 2em;
  :hover {
    cursor: pointer;
  }
`;

const FansModal = ({isOpen, onClose}) => {

    return (
        <Modal ariaHideApp={false} shouldCloseOnOverlayClick={true} isOpen={isOpen} style={{content:{textAlign: 'center'}}}>
          <Close onClick={onClose}>close</Close>  
          <h1>My Fans</h1>
          <FansListing />
        </Modal>
    );
}

export default FansModal;