import React from "react";
import concert from "../Assets/images/concert.png";
import styled from 'styled-components';

const Section = styled.section`

    height: 100%;
    display: block;
    
    .section__title {
    position: relative;
    padding: 3em 0 3em 0;
    margin: 0 auto;
      h1 {
        background: -webkit-linear-gradient(#62a2ad, #8dbab3);
        -webkit-background-clip: text;
       -webkit-text-fill-color: transparent;
      }
    }
    .section__inside__wrap {
    position: absolute;
    width: 100%;
    bottom: 5em;
    left: 0;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    }
    .section__inside {
    position: relative;
    justify-content: center;
    }
    
    #section__img__wrap {
    position: static;
    display: inline-block;
    overflow: visible;
    left: 0;
    bottom: 0;
    height: 35em;
      img {
        object-fit: cover;
        display: block;
        height: 28em;
        bottom: 0;
        left: 0;
        width: 100%;
      }
    }
    
    @media (min-width: 640px){ 
    .section__title {
        width: 500px;
      }
    }
    
    @media(min-width: 1024px) {
    #section__img__wrap {
        position: relative;
        height: 45em;
      img {
        height: 100%;
      }
    }
     .section__title {
        width: 600px;
      }
    }
    
    @media(min-width: 1800px) {
    #section__img__wrap {
      height: 1000px;
      img {
        object-position: 50% 100%;
      }
    }
    .section__inside__wrap {
        bottom: 18em;
    }
      .section__title {
        width: 800px;
      }
    }
`;

const Tour = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-content: center;
    width: 100%;
    bottom: 0;
    text-align: left;
    background: rgba(255,255,255, .5);
    padding: 30px;
    box-sizing: border-box;
    
    .content__year {
      flex-direction: column;
      padding-bottom: .5em;
      ::after {
        top: 5.5em;
      }
    }
    
    @media(min-width: 480px) {
      .content__year {
        flex-direction: row;
          ::after {
            top: 4.5em;
          }
      }
    }
    @media(min-width: 1024px) {
        background: none;
        padding: 0;
      .content__description {
        width: 70%;
      }
    }
    @media(min-width: 1800px) {
       .content__description {
          width: 60%;
       }
    }
`;

const ConcertToursSection = () => {

    return (
        <Section id="concerttours">
            <div className="section__title">
                <h1>
                    concert tours
                </h1>
                <p className="section__subtitle">
                    Before the release of Night Visions, Imagine Dragons made appearances on American radio and television to promote
                    their extended play, Continued Silence and debut single It's Time. The band performed "It's Time" on the
                    July 16, 2012 airing of NBC late-night talk show The Tonight Show with Jay Leno"
                </p>
            </div>
            <div className="section__inside__wrap">
                <div className="section__inside">
                    <Tour>
                <div className="content__year">
                    <h4>03.08.2015</h4>
                    <hr />
                    <h3>Smoke + Mirrors tour</h3>
                </div>
                <div className="content__description">
                    <h2>2015 - present</h2>
                        <p>
                           At Lollapalooza in Sao Paulo, Brazil, the last date on the Into the Night Tour, the band announced a rest,
                            and complemented saying, "This is our last show for a while, and had no better place to end this tour".[51]
                            The conclusion of the Into the Night Tour signaled the end of the Night Visions album cycle.
                            Lead singer Dan Reynolds joked about the end of the Night Visions cycle, saying that "We're always writing on the road,
                            [so] that second album will come, unless we die.
                        </p>
                    <button className="btn btn__arrow"><span>Buy online</span><i></i></button>
                </div>
                </Tour>
                </div>

            </div>
            <div id="section__img__wrap">
                    <img src={concert} alt="discography-bg" />
                </div>

        </Section>
    );
}

export default ConcertToursSection;