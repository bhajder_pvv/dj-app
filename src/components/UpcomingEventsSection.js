import React from 'react';
import styled from 'styled-components';
import bg from '../Assets/images/events.jpg';

const Section = styled.section`
    background: url(${bg}) no-repeat center left;
    height: 75em;
    margin-top: 10em;
    background-size: cover;

    h2 {
      margin: 2em 0;
    }
    
    .section__inside {
     align-content: center;
    }
    
    .content__year {
      display: flex;
      flex-direction: column;
      align-items: center;
      width: 100%;
      padding: 0;
    }
    
    .content__description {
      width: 100%;
    }
    
    @media(min-width: 640px) {
      h2 {
        margin: 1em 0;
      }
    }
    
    @media(min-width: 1024px) {
      .content__year {
        display: block;
        position: relative;
        left: 0;
      }
    }
    
    @media(min-width: 1600px) {
      .section__inside {
        width: 600px;
        right: 20em;
        display: block;
        position: absolute;
      }
    }
`;

const Event = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    align-content: center;
    flex-direction: column;
    background: rgba(255, 255, 255, .5);
    box-sizing: border-box;
    padding: 0 30px;
    width: 100%;
    
    :last-child {
      padding-bottom: 30px; 
      .spacer {
        display: none;
      }
    }
    
    @media(min-width: 1024px) {
      align-items: flex-start;
    
      .spacer {
        left: 2em;
        height: 10em;
      }
    }
    
    @media(min-width: 1600px) {
      background: none;
    }
`;

const Spacer = styled.div`
   width: 1px;
   height: 4em;
   background: #000;
   margin: 1em 0;
   position: relative;
   left: 0;
   
   @media(min-width: 1024px) {
    left: 2em;
   }
`;

const eventsData = [{
    id: 1,
    name: 'Smoke + Mirrors Tour',
    description: '2-2-15 Hokukoryokuchi, Konohana Ward,' +
        '554-0042 Osaka Prefecture Osaka, ' +
        'Japan',
    date: '1.04.2015'
}, {
    id: 2,
    name: 'Smoke + Mirrors Tour',
    description: 'When the last-minute request is for a focus group, it’s usually a sign that the ' +
        'request originated in Marketing. When Web sites are being designed, the folks in ' +
        'Marketing often feel like they don’t have much clout. Even though they’re the ' +
        'ones who spend the most time trying to figure out who the site’s audience is and ' +
        'what they want, the designers and developers are the ones with most of the',
    date: '27.05.2015'
}]

const UpcomingEventsSection = () => {
    return (
        <Section id="upcomingevents">
            <div className="section__inside">
                <h1>upcoming events</h1>
                {eventsData.map(event => <Event key={event.id}>
                    <div className="content__description">
                        <h2>{event.name}</h2>
                        <p>{event.description}</p>
                    </div>
                    <div className="content__year">
                        <Spacer/>
                        <h3>{event.date}</h3>
                    </div>
                    <Spacer className="spacer" />
                </Event>)}
            </div>
        </Section>
    )
};

export default UpcomingEventsSection;