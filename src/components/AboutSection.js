import React from 'react';
import Navigation from './Navigation';
import styled from 'styled-components';
import logotype from '../Assets/images/logotype.png'
import dj from '../Assets/images/dj.png';

const Header = styled.header`
    min-height: 600px;
    width: 100%;
    position: relative;
    top: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 0;
    
    #header__logotype {
        position: absolute;
        z-index: 1;
        width: 200px;
        height: auto;
    }
    
    #header__img {
        width: 100%;
        z-index: 0;
        object-fit: cover;
        height: 650px;
    }
    
    @media(min-width: 1440px) {
      #header__logotype {
      width: 20vw;
      }
      #header__img {
      object-fit: contain;
      height: auto;
      }
    }
`;

const AboutSection = () => {

    return (
        <Header id="about">
            <Navigation  />
                <img id="header__logotype" src={logotype} alt="logo" />
                <img id="header__img" src={dj} alt="dj" />
        </Header>
    );
}

export default AboutSection;