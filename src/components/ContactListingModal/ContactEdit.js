import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import ContactForm from "../ContactForm";
import {editContact} from "../../actions/contact";

const ContactEdit = ({contacts, editContact, id, modeHandler}) => {
    const contactToEdit = contacts.map(contact => {
            if(contact.id == id) return contact;
    });

    const filtered = contactToEdit.filter(el => {
        return el != null;
    });

    const handleOnSubmit = (contact) => {
        editContact(id, contact);
        modeHandler();
    };

    return (
        <div>
            <ContactForm contactData={filtered[0]} onSubmit={handleOnSubmit} />
        </div>
    );
}

const mapStateToProps = (state) => ({
    contacts: state.contacts
});

const mapDispatchToProps = (dispatch) => ({
    editContact: (id, updates) => dispatch(editContact(id, updates))
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactEdit);