import React, { useState } from 'react';
import Modal from 'react-modal';
import styled from "styled-components";
import ContactListing from "./ContactListing";
import ContactEdit from "./ContactEdit";

const Close = styled.div`
    position: absolute;
    right: 2em;
    :hover {
      cursor: pointer;
    }
    ::before, ::after {
      content: "";
      background: #000;
    }
`;

const Back = styled.div`
  position: absolute;
  left: 2em;
  :hover {
    cursor: pointer;
  }
`;

const ContactListingModal = ({isOpen, onClose}) => {
    const [mode, setMode] = useState('listing');
    const [editId, setEditId] = useState('');

    const handleSetEditId = (id) => {
        setEditId(id);
        setMode('edit');
    };

    return (
        <Modal ariaHideApp={false} shouldCloseOnOverlayClick={true} isOpen={isOpen} style={{content:{textAlign: 'center'}}}>
            <Close onClick={onClose}>close</Close>
            {(mode == 'edit') && <Back onClick={() => setMode('listing')}>back</Back>}
            <h1>CONTACT LISTING</h1>
            {(mode == 'edit') ? <ContactEdit id={editId} modeHandler={() => setMode('listing')} /> : <ContactListing idHandler={handleSetEditId} />}
        </Modal>
    );
}

export default ContactListingModal;