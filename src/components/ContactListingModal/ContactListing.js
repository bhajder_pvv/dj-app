import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import {faPen} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const Listing = styled.div`
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
`;

const Contact = styled.div`
  position: relative;
  text-align: left;
  width: 30em;
  height: 15em;
  padding: 2em;
  background: #f8f8f8;
  margin: 2em;
  p {
  margin-top: 1em;
    color: #111;
  }
`;

const Edit = styled.span`
  position: absolute;
  right: 2em;
  top: 2em;
  color: #999;
  transition: color .1s ease;
  :hover {
    color: #000;
    cursor: pointer;
    transition: color .3s ease;
  }
`

const ContactListing = ({contacts, idHandler}) => {

    return (
        <Listing>
            {
                contacts.map(contact =>
                    <Contact key={contact.id}>
                        <Edit><FontAwesomeIcon icon={faPen} onClick={() => {idHandler(contact.id)}} /></Edit>
                        <h2>{contact.name}</h2>
                        <p>{contact.date}</p>
                        <h3>{contact.email}</h3>
                        <p>{contact.message}</p>
                    </Contact>
                )
            }
        </Listing>
    );
}

const mapStateToProps = (state) => ({
    contacts: state.contacts
});

export default connect(mapStateToProps)(ContactListing);