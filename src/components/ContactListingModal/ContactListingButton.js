import React from 'react';
import styled from 'styled-components';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAddressCard} from "@fortawesome/free-solid-svg-icons";

const Button = styled.button`
    bottom: 6em;
    z-index: 999;
    :hover {
      background: #000 !important;
    }
    @media(min-width: 1024px) {
      display: block;
    }
`;

const ContactListingButton = (props) => {

    const handleParentChange = () => {
        props.onButtonClick();
    };

    return  (
        <Button onClick={handleParentChange} className="btn btn__icon">
            <FontAwesomeIcon icon={faAddressCard} />
        </Button>
    );
}

export default ContactListingButton;
