import React, {useState, useEffect} from 'react'
import { connect } from 'react-redux';
import styled, { keyframes } from 'styled-components';
import TextTruncate from "react-text-truncate";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faComment, faHeart, faVolumeUp} from "@fortawesome/free-solid-svg-icons";
import modify from "../modifiers/statistics";

const loadingAnimation = keyframes`
     from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
`;

const Composition = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: center;
    background: #f5f6f6;
    box-sizing: border-box;
    margin-top: 3em;
    animation-duration: .5s !important;
    
    .hide {
      visibility: hidden;
    }
    
    .content__left {
      padding: 30px;
       ::after {
        display: block;
        position: relative;
        z-index: 99;
        bottom: calc(-30px - 1em);
        content: "";
        width: 0;
        height: 0;
        border-style: solid;
        border-width: 1em 1em 0 1em;
        border-color: #f5f6f6 transparent transparent transparent;
        left: 50%;
      }
    }
    
    .content__right {
        height: 280px;
        display: flex;
        justify-content: center;
        align-items:center;
        ::after {
          content: "";
          box-sizing: border-box;
          ${props => props.showLoader ? "display: block" : "display: none"};
          position: absolute;
          width: 51px;
          height: 51px;
          margin: 6px;
          border: 6px solid #000;
          border-radius: 50%;
          animation: ${loadingAnimation} 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
          border-color: #000 transparent transparent transparent;
        }
    }
    
    .btn {
      margin: 1em 0 0 0;
      width: 100%;
    }
    
    .content__footer {
      display: flex;
      flex-direction: column-reverse;
      align-items: center;
      width: 100%;
    }
    
    .stats {
      width: 100%;
      display: flex;
      justify-content: space-between;
    }
    
    iframe {
      z-index: 2;
    }
    
    @media(min-width: 480px) {
      .content__footer {
        flex-direction: row;
      }
      .btn {
        width: 180px;
        margin: 0 1em 0 0;
      }
    }
    
    @media(min-width: 1024px) {
    height: 360px;
    :nth-child(even) {
      flex-direction: row;
      .content__left {
        ::after {
          position: absolute;
          left: auto;
          bottom: 50%;
          right: -1em;
          border-width: 1em 0 1em 1em;
          border-color: transparent transparent transparent #f5f6f6;
        }
      }
    }
    
    :nth-child(odd) {
      flex-direction: row-reverse;
      .content__left {
        ::after {
        position: absolute;
          right: auto;
          bottom: 50%;
          left: -1em;
          border-width: 1em 1em 1em 0;
          border-color: transparent #f5f6f6 transparent transparent;
        }
      }
    }
    
    h2 {
      margin: 0 0 1em 0;
    }
    .content__right {
      padding: 0;
      height: auto;
    }
    .content__right, .content__left {
       position: relative;
       width: 50%;
       box-sizing: border-box;
    }
    .content__year {
        width: 70%;
    } 
    .content__description {
        width: 100%;
    }
    .content__footer {
      width: calc(100% - 60px);
      position: absolute;
      align-items: flex-end;
      margin-top: 2em;
      bottom: 30px;
    }
    .btn {
        height: 40px;
        margin: 0 1em 0 0;
     }
    }

    @media(min-width: 1800px) {
      height: 400px;
      .stats {
        width: 22em;
      }
    }
`;
const LatterCompositionsList = ({compositionsData, newCompositionData, isNew}) => {
    const [currentDataSource, setCurrentDataSource] = useState(compositionsData);

    useEffect(() => {
        isNew ? setCurrentDataSource(newCompositionData) : setCurrentDataSource(compositionsData);
    }, [newCompositionData, compositionsData]);

    return (
        <div>
        {currentDataSource ? currentDataSource.map(composition => composition && <Composition showLoader={Boolean(composition.id)} key={composition.id} className="wow fadeIn">
                <div className="content__left">
                    <div className="content__year">
                        <h4>{composition.date.slice(0,composition.date.indexOf('T')).replace(/-/g, '.')}</h4>
                        <hr />
                        <h3>{composition.channelTitle}</h3>
                    </div>
                    <div className="content__description">
                        <h2>{composition.title}</h2>
                        <TextTruncate
                            line={3}
                            truncateText="…"
                            text={composition.description ? composition.description : 'Here goes your description'}
                        />
                        <div className="content__footer">
                            <button className="btn btn__arrow"><span>visit itunes</span><i></i></button>
                            <div className="stats">
                                <span><FontAwesomeIcon icon={faVolumeUp} />{composition.viewCount ? modify(composition.viewCount) : "-"}</span>
                                <span><FontAwesomeIcon icon={faHeart} />{composition.likeCount ? modify(composition.likeCount) : "-"}</span>
                                <span><FontAwesomeIcon icon={faComment} />{composition.commentCount ? modify(composition.commentCount) : "-"}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="content__right">
                    {composition.id && <iframe title={composition.id} src={`https://www.youtube.com/embed/${composition.id}`}
                            frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen
                    />}
                </div>
            </Composition>) : 'Nothing to show yet ;('}
            </div>
    );
}

const mapStateToProps = (state) => {
    return {
        compositionsData: state.compositions
    }
};

export default connect(mapStateToProps)(LatterCompositionsList);