import React, { useState, useEffect } from 'react';
import styled from 'styled-components';

const Section = styled.section`
  position: absolute;
  height: auto;
  z-index: 1;
  text-align: left;
`;

const Video = styled.div`
   display: flex;
   flex-direction: row;
   width: 100%;
   align-items: center;
   height: 7em;
   :hover {
    cursor: pointer;
   }
   img {
    height: 6em;
    object-fit: contain;
   }
   h2 {
    margin-left: 1em;
    font-size: 1.2em;
   }
`;

const VideoList = ({ data, limit, onCurrentVideoLoad }) => {

    const [currentVideo, setCurrentVideo] = useState({
        id: '',
        url: '',
        title: '',
        date: '',
        viewCount: 0,
        channelTitle: '',
        likeCount: 0,
        commentCount: 0,
    });

    useEffect(() => {
        onCurrentVideoLoad(currentVideo);
    }, [currentVideo]);

    return (
        <Section>
            {limit ? <p>OOPS! Search limit has been reached ;( Try again in a few minutes</p>:
                data.map(video => video &&
                <Video key={video.id} onClick={() => {
                    setCurrentVideo({
                        id: video.id,
                        title: video.snippet.title,
                        date: video.snippet.publishedAt,
                        url: `https://www.youtube.com/watch?v=${video.id}`,
                        viewCount: video.statistics.viewCount,
                        channelTitle: video.snippet.channelTitle,
                        commentCount: video.statistics.commentCount,
                        likeCount: video.statistics.likeCount,
                    });
                }
                }>
                    <img src={video.snippet.thumbnails.medium.url} alt="thumb"/>
                    <div>
                        <h2>{video.snippet.title}</h2>
                    </div>
                </Video>
            )}
        </Section>
    );
}

export default VideoList;