import React, {useState} from 'react';
import styled from 'styled-components';
import NavList from "./NavList";
import Breakpoint, { setDefaultBreakpoints } from 'react-socks';

setDefaultBreakpoints([
  { small: 0 },
  { large: 1440 }
]);

const Nav = styled.nav`
      position: absolute;
      top: 0;
      width: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
      transition: all .5s ease;
      z-index: 9999999999;
      
      .hamburger__wrapper {
        position: absolute;
        top: 20px;
        right: 20px;
        width: 60px;
        height: 45px;
        z-index: 999999999999;
        ${props => !props.active && "background: rgba(255, 255, 255, .8);"};
        box-sizing: border-box;
        display: flex;
        justify-content: center;
        align-items: center;

         :hover {
          cursor: pointer;
         }
      }
      
      @media(min-width: 1440px) {
        width: 100%;
        height: 70px;
        background-color: rgba(255, 255, 255, .9);
        z-index: 999;
      }
    
`;

const Hamburger = styled.div`
      
      width: 40px;
      height: 4px;
      background: #000;
      position: relative;
      z-index: 9999999999999;
      border-radius: 2px;
      ${props => props.active && "transform: rotate(-45deg);"};
      transition: all .12s ease;
      
      ::before {
      display: block;
      top: -10px;
      width: 40px;
      height: 4px;
      background: #000;
      position: absolute;
      z-index: 9999999999999;
      content: "";
      border-radius: 2px;
      ${props => props.active && "transform: rotate(-90deg); top: 0;"};
      transition: all .12s ease;
      }
      
      ::after {
      display: block;
      top: 10px;
      width: 40px;
      height: 4px;
      background: #000;
      position: absolute;
      z-index: 9999999999999;
      content: "";
      border-radius: 2px;
      ${props => props.active && "top: 0; opacity: 0;"};
      transition: all .12s ease;
      }
      
      @media(min-width: 1440px) {
        display: none;
      }
`;

const SlideNav = styled.div`
      position: fixed;
      display: flex;
      align-items: center;
      justify-content: center;
      top: 0;
      right: 0;
      width: 100%;
      height: 100%;
      z-index: 999;
      ${props => props.active ? "transform: translateX(0);" : "transform: translateX(100%);"};
      transition: all .12s ease;
      transition-delay: .3s;
      background: rgba(255, 255, 255, .9);
      
      ::after {
      z-index: -1;
      position: absolute;
      content: "";
      width: 100%;
      height: 100%;
      top: 0;
      }
`;

// const Backdrop = styled.div`
//       width: 100%;
//       height: 100%
//       position: fixed;
//       left: 0;
//       ${props => props.active ? "opacity:1;" : "opacity:0;"};
//       background: rgba(0, 0, 0, .3);
//       z-index: 3;
//       transition: all .3s ease;
//       overflow: hidden;
//
//       ::after {
//         width: 100%;
//         height: 100%;
//         display: block;
//         position: relative;
//         background: #000;
//         opacity: .5;
//         content: "";
//       }
// `;

const Navigation = () => {

    const [isActive, changeIsActive] = useState(false);

    const handleMobileNav = () => {
        changeIsActive(!isActive);
        if(isActive) {
            document.body.style.overflow = "scroll";
        } else {
            document.body.style.overflow = "hidden";
        }
    }

    return (
    <Nav id="home" active={Boolean(isActive)}>
        <Breakpoint small only>
            <div className="hamburger__wrapper" onClick={handleMobileNav}>
                <Hamburger className="animated fadeIn" active={Boolean(isActive)} />
            </div>
            <SlideNav active={Boolean(isActive)}>
                <NavList isMobile="true" handler={handleMobileNav}/>
            </SlideNav>
            {/*{Boolean(isActive) && <Backdrop active={Boolean(isActive)} onClick={handleMobileNav} />}*/}
        </Breakpoint>
        <Breakpoint className="animated fadeIn" large up>
            <NavList  />
        </Breakpoint>
    </Nav>
    )
};

export default Navigation;