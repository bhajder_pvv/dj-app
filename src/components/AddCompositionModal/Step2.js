import React from 'react';
import { connect } from 'react-redux';
import LatterCompositionsList from "../LatterCompositionsList";
import styled from "styled-components";
import {addComposition} from "../../actions/compositions";

const Preview = styled.div`
  
`;

const DoneContent = styled.div`
    display: flex;
    width: 100%;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    ${Preview} {
      width: 1200px;
      text-align: left;
      top: 2em;
    }
    .done__buttons {
      position: relative;
      top: 2em;
      width: 600px;
      display: flex;
      justify-content: space-around;
    }
`;

const Step2 = (props) => {

    const handleDispatch = () => {
        const currentState = props.currentState;
        const compositionId = currentState.map(cn => cn.id );
        compositionId.find((id) => {
            if(id === props.template.id) {
                throw alert("This id already exists");
            }
        });
        props.addComposition(props.template);
        props.changeStep(3, props.template);
    };

    return (
        <div>
            <h1>Confirm your composition</h1>
            <DoneContent>
                <Preview>
                    <LatterCompositionsList isNew={true} newCompositionData={[props.template]}/>
                </Preview>
                <div className="done__buttons">
                    <button className="btn btn__simple btn__black" onClick={() => {props.changeStep(1, props.template)}}>back</button>
                    <button className="btn btn__simple" onClick={handleDispatch}>confirm</button>
                </div>
            </DoneContent>
        </div>
    );
}

const mapStateToProps = (state) => ({
    currentState:  state.compositions
});

const mapDispatchToProps = (dispatch) => ({
    addComposition: (composition) => dispatch(addComposition(composition))
});

export default connect(mapStateToProps, mapDispatchToProps)(Step2);