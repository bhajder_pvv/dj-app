import React from 'react';
import LatterCompositionsList from "../LatterCompositionsList";
import VideoList from "../VideoList";
import styled from "styled-components";
import snippet from "../../youtube/snippet";
import search from "../../youtube/search";

const Left = styled.div`
  width: 70%;
  justify-content: center;
  margin-right: 5em;
  height: 100%;
  text-align: left
`;

const Right = styled.div`
    position: relative;
    width: 30%;
    margin-top: 3em;
    p {
      margin-top: 2em;
    }
`;

const ModalContent = styled.div`
    display: flex;
    flex-direction: row;
    width: 100%;
    height: auto;
    input, textarea, form {
      display: block;
      width: 100%;
    }
    textarea {
      resize: none;  
    }
    
    input {
      height: 3em;
      border-width: 0 0 2px 0;
    }
`;

const Preview = styled.div`
  
`;

class Step1 extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            videos: [],
            limit: false,
            searchTimeout: 0,
            template: this.props.template
        }
    }

    componentDidMount = () => {
        this.setState({
            template: this.props.template
        });
    }

    handleVideos = (ids) => {
        ids.map(id => {
            snippet(id).then((youtube) => {
                this.setState({videos: [...this.state.videos, youtube.data.items[0]]});
                console.log(this.state);
            });
        });
    }

    handleSearch = (term) => {
        search(term).then((value)=> {
            this.handleVideos(value);
        }).catch((err) => {
            this.setState({limit: true})
        });
    }

    onCurrentVideoLoad = (data) => {
        this.setState({template: {...data, description: ''}});
    }

    handleSearchChange = (event) => {
        const term = event.target.value;
        if(this.state.searchTimeout) clearTimeout(this.state.searchTimeout);
        this.setState(prevState => ({
            ...prevState,
            videos: [],
            searchTimeout: setTimeout(() => {
                    this.handleSearch(term);
                    }, 500)
        }));
    }

    handleOnDescriptionChange = (event) => {
        const description = event.target.value;
        this.setState(prevState => ({template: {...prevState.template, description: description}}))
    }

    render(){
        return (
            <div>
                <h1>Add a new composition</h1>
                <ModalContent>
                    <Left>
                        <input type="text" onChange={this.handleSearchChange} placeholder="search on youtube"/><br/>
                        <Preview>
                            <h3>Preview:</h3><LatterCompositionsList isNew={true} newCompositionData={[this.state.template]}/>
                        </Preview>
                        <br/>

                        {this.state.template.id &&
                        <div>
                            <label htmlFor="description"><h3>Add some description:</h3></label><br/>
                            <textarea name="" id="description" cols="30" rows="10"
                                    onChange={this.handleOnDescriptionChange}
                                    value={this.state.template.description}
                            />
                            <button className="btn btn__simple" onClick={() => {this.props.changeStep(2, this.state.template)}}>next
                            </button>
                        </div>
                        }<br/>
                    </Left>
                    <Right>
                        <h3>Choose from youtube videos:</h3>
                        {this.state.videos.length >= 1 || this.state.limit ?
                            <VideoList data={this.state.videos} limit={this.state.limit} onCurrentVideoLoad={this.onCurrentVideoLoad}/> :
                            <p>There is nothing to show yet, Pal!</p>}
                    </Right>
                </ModalContent>
            </div>
        )
    }
};

export default Step1;