import React from 'react';
import styled from 'styled-components';
import Modal from 'react-modal';
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";

const Close = styled.div`
    position: absolute;
    right: 2em;
    :hover {
      cursor: pointer;
    }
    ::before, ::after {
      content: "";
      background: #000;
    }
`;

const defaultState = {
    step: 1,
    template: {
        id: '',
        url: '',
        title: '',
        date: '',
        viewCount: 0,
        channelTitle: '',
        likeCount: 0,
        commentCount: 0,
        description: ''
    }
}
class AddCompositionModal extends React.Component {

    constructor(props){
        super(props);
        this.state = defaultState
    }

    render() {

        const handleChangeStep = (step, passedTemplate) => {
                this.setState(prevState => ({...prevState, step: step, template: passedTemplate}))
        };

        return (
            <Modal ariaHideApp={false} shouldCloseOnOverlayClick={true} isOpen={this.props.isOpen} done={this.state.step} style={{content:{textAlign: 'center'}}}>
                <Close onClick={this.props.onClose}>close</Close>
                {
                    (() => {
                        switch(this.state.step){
                            case 1: return (
                                    <Step1
                                        changeStep={handleChangeStep}
                                        template={this.state.template}
                                    />
                            );

                            case 2: return (
                               <Step2
                                   changeStep={handleChangeStep}
                                   template={this.state.template}
                               />
                            );

                            case 3: return (
                                <Step3
                                    changeStep={handleChangeStep}
                                    onClose={this.props.onClose}
                                />
                            );
                            default: return (
                                <Step1
                                    changeStep={handleChangeStep}
                                    template={this.state.template}
                                />
                            )
                        }
                    })()
                }
            </Modal>
        )
    }
}

export default AddCompositionModal;