import React from 'react';
import styled from "styled-components";

const Finish = styled.div`
    
`;

const defaultTemplate = {
        id: '',
        url: '',
        title: '',
        date: '',
        viewCount: 0,
        channelTitle: '',
        likeCount: 0,
        commentCount: 0,
        description: ''
}

const Step3 = (props) => {
    return (
        <Finish>
            <h1>We're done!</h1>
            <h2>Your composition has been added successfully!</h2>
            <div className="done__buttons">
                <button className="btn btn__simple" onClick={() => {props.changeStep(1, defaultTemplate)}}>Add Another</button>
                <button className="btn btn__simple" onClick={props.onClose}>Close</button>
            </div>
        </Finish>
    );
};

export default Step3;