import React from 'react';
import styled from 'styled-components';
import leak from '../Assets/images/leak1.jpg';
import discography from '../Assets/images/discography.png';
import TextTruncate from 'react-text-truncate';
import btn1 from '../Assets/images/btn1.jpg';
import btn2 from '../Assets/images/btn2.jpg';
import btn3 from '../Assets/images/btn3.jpg';

const Section = styled.section`
  
    .section__subtitle {
      margin-bottom: 3em;
    }
  
    h1 {
     background: url(${leak}) no-repeat center center;
     color: transparent;
     background-clip: text;
     -webkit-background-clip: text;
    }

    #section__img__wrap {
      display: inline-block;
      position: absolute;
      overflow: visible;
      bottom: 0;
    
    img {
      object-fit: cover;
      display: block;
      height: 100%;
      max-width: 100%;
     }
    }
    
    @media(min-width: 1440px) {
      .section__subtitle {
        margin-bottom: 7em;
      }
    }
`;

const Disk = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-content: center;
    width: 100%;
    padding: 30px;
    margin-top: 4em;
    box-sizing: border-box;
    animation-duration: .5s !important;
    
    @media(min-width: 1024px) {
      flex-direction: row;
    }
    
    @media(min-width: 1440px) {
        padding: 4em 0;
    }
`;

const Button = styled.button`
      position: relative;
      background: url(${props => props.bgPicture && props.bgPicture}) no-repeat center center;
      background-size: cover;
      border: none;
      
      ::after {
        display: block;
        position: absolute;
        width: 100%;
        height: 100%;
        content: "";
        background: #000;
        opacity: 0;
        top: 0;
        left: 0;
        transition: opacity .2s ease;
      }
      
      :active::after {
        opacity: .4;
      }
      
      :hover::after {
        opacity: .2;
      }
`;

const Clearfix = styled.div`
  height: 25em;
  width: 100%;
`;

const disksData = [{
    id: 1,
    title: 'Hell and Silence is an EP by Las Vegas rock group',
    description: 'Hell and Silence is an EP by Las Vegas rock group , released in March 2010 in the United States.' +
        'It was recorded at Battle Born Studios. All songs were written by Imagine Dragons and self-produced.' +
        'The EP was in part mixed by Grammy nominated engineer Mark Needham. To promote the album the band performed' +
        'five shows at SXSW 2010 including at the BMI Official Showcase.While at SXSW they were endorsed by Blue Microphones.' +
        'They also toured the western United States with Nico Vega and Saint Motel. They also performed at Bite of Las Vegas Festival 2010,' +
        'New Noise Music Festival, Neon Reverb Festival, and Fork Fest.',
    year: '2010',
    name: 'Hell and Silence'
}, {
    id: 2,
    title: 'Night Visions is the debut studio album by American rock band',
    description: 'It was released on September 4, 2012 through Interscope Records. The extended track was released on February 12, 2013, ' +
        'adding three more songs. Recorded between 2010 and 2012, the album was primarily produced by the band themselves, ' +
        'as well as English hip-hop producer Alex da Kid and Brandon Darner from the American indie rock group The Envy Corps. ' +
        'It was mastered by Joe LaPorta. According to frontman Dan Reynolds, the album took three years to finish ...',
    year: '2012',
    name: 'Night Visions'
}, {
    id: 3,
    title: 'The album was recorded during 2014' +
        'at the band\'s home studio in Las Vegas, Nevada',
    description: 'Self-produced by members of the band along with English hip-hop producer Alexander Grant, ' +
        'known by his moniker Alex da Kid, the album was released by Interscope Records and Grant\'s KIDinaKORNER ' +
        'label on February 17, 2015, in the United States.',
    year: '2015',
    name: 'Smoke + Mirrors'
}];

const btnBgPictures = [btn1, btn2, btn3];

const DiscographySection = () => {

    return(
    <Section id="discography">
        <div className="section__inside">
            <div className="section__title">
                <h1>
                discography
                </h1>
            </div>
            <p className="section__subtitle">
                September 4 world heard Night Visions, the first full album.
                He reached the 2 position in the chart Billboard 200.
                The single «It's Time» took 22 th place in the Billboard Hot 100,
                4th in the Billboard Alternative and Billboard Rock, and now went platinum.
            </p>
            {disksData.map(disk => <Disk key={disk.id}>
                <div className="content__year">
                    <h4>{disk.year}</h4>
                    <hr />
                    <h3>{disk.name}</h3>
                </div>
                <div className="content__description">
                    <h2>{disk.title}</h2>

                            <TextTruncate
                            line={2}
                            truncateText="…"
                            text={disk.description}
                            />

                <Button className="btn btn__simple" bgPicture={btnBgPictures[Math.floor(Math.random()*btnBgPictures.length)]}>play</Button>
                </div>
            </Disk>)}
        </div>
        <div id="section__img__wrap">
            <img src={discography} alt="discography-bg" />
        </div>
        <Clearfix/>
    </Section>
    );
}

export default DiscographySection;