import React from 'react';
import Slider from 'react-slick';
import styled from 'styled-components';
import history from "../Assets/images/history.jpg";

const Section = styled.section`
  justify-content: flex-start;
  h1::before {
    display: none;
  }
  .section__inside {
    position: absolute;
    width: 100%;
    top: 17em;
  }
  .section__title {
    position: absolute;
    padding: 0 30px 30px 30px;
  }
  #section__img__wrap { 
    height: 60em;
    position: absolute;
    img {
      object-fit: cover;
      object-position: 50% 0;
      height: 100%;
      width: 100%;
    }
  }
   .slick-current {
      h1{
       ::before, ::after {
         position: absolute;
         content: "•";
         display: block;
         background: none;
         top: 0;
         color: #798285;
       }
       ::before {
       left: -3.5em;
       }
       ::after {
       right: -2em;
       }
     }
     p {
       display: block;
     }
   }
   
   @media(min-width: 1024px) {
   .section__inside {
    top: 20em;
   }
      #section__img__wrap { 
        height: 75em;
      }
   }
`;

const Entry = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    text-align: center;
    
    :hover {
      cursor: pointer !important;
    }
    :focus {
      outline: none;
    }
    h1 {
      font-size: 2.5em;
      padding: 0;
    }
    p {
      display: none;
      box-sizing: border-box;
      width: 17em;
    }
`;

const Clearfix = styled.div`
    height: 60em;
    
    @media(min-width: 1024px) {
      height: 75em;
    }
`;

const historyData = [{
    id: 1,
    date: 'apr 11',
    description: 'Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys.'
}, {
    id: 2,
    date: 'mar 16',
    description: 'Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys.'
},{
    id: 3,
    date: 'may 09',
    description: 'Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys.'
},  {
    id: 4,
    date: 'jun 06',
    description: 'Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys.'
}, {
    id: 5,
    date: 'jul 25',
    description: 'Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys.'
}, {
    id: 6,
    date: 'sep 04',
    description: 'Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys.'
}, {
    id: 7,
    date: 'oct 30',
    description: 'Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys.'
} ,{
    id: 8,
    date: 'dec 12',
    description: 'Tolman later recruited his wife, Brittany Tolman, to sing backup and play keys.'
}];

const HistorySection = () => {

    const settings = {
        arrows: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        draggable: true,
        centerMode: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 1601,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    };

    return (
        <Section id="history">
            <div className="section__title">
                <h1>history</h1>
                <p className="section__subtitle">
                In 2008, lead singer Dan Reynolds met drummer Andrew Tolman at Brigham Young University where they were both students.
                Tolman recruited longtime high school friend Daniel Wayne "Wing" Sermon, who had graduated from Berklee College of Music
                </p>
            </div>
            <div className="section__inside">
                <Slider {...settings}>
                {historyData.map(entry => <Entry key={entry.id}>

                        <h1>{entry.date}</h1>
                        <p>{entry.description}</p>
                </Entry>)}
                </Slider>
            </div>
            <div id="section__img__wrap">
                <img src={history} alt="history-bg" />
            </div>
            <Clearfix/>
        </Section>
    );
}

export default HistorySection;