import React from 'react';
import styled from 'styled-components';

const InputWrapper = styled.div`
    height: 4.8em;
`;

const Input = ({input, meta: { touched, error }, type, name, placeholder}) => (
    <InputWrapper>
        <input {...input} type={type} name={name} placeholder={placeholder}/>
        {touched && error && <span>{error}</span>}
    </InputWrapper>
)

export default Input;