import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

const Datepicker = ({input}) => {
    const [date, setDate] = useState(new Date());

    return (
        <span {...input}>
        <DatePicker
            selected={date}
            onChange={setDate}
            value={date}
        />
        </span>
    );
}

export default Datepicker;