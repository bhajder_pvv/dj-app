import React from 'react';

const Textarea = ({onDescriptionChange}) => (
    <div>
        <textarea onChange={(event) => {onDescriptionChange(event)}}></textarea>
    </div>
);

export default Textarea;