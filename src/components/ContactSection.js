import React, { useState } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import contact from '../Assets/images/contact.jpg';
import ContactSectionForm from "./ContactForm";
import ReactCSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import { addContact } from "../actions/contact";

const Footer = styled.footer`
    display: flex;
    position: relative;
    position: absolute;
    justify-content: center;
    height: 100%;
    width: 100%;
    
    span {
      color: orangered;
      position: relative;
      right: 0;
      top: .5em;
    }
    
    button {
      text-transform: uppercase;
      font-family: 'open_sansbold';
      font-weight: bold;
      background: none;
      transition: opacity .12s ease;
      width: auto;
      font-size: 1.2em;
      :hover {
      cursor: pointer;
      opacity: .6;
      transition: opacity .12s ease;
      }
    }
    
    .section__title {
      h1 {
        text-align: left;
        width: 100%;
        padding-left: 0;
        padding-bottom: 1em;
        ::before {
        left: 0;
        width: 50%;
        }
      }
      p {
        text-align: left;
      }
    }
    .section__inside {
      padding: 0 30px;
      top: 5em;
      display: block;
      position: absolute;
      box-sizing: border-box;
    }
    #section__img__wrap {
      position: absolute;
      height: 100%;
      bottom: 0;
      img {
        object-fit: cover;
        height: 100%;
      }
    }
    
    @media(min-width: 640px) {
        .section__inside {
          padding: 0;
        }
    }
    
    @media(min-width: 1024px) {
        .section__title {
          h1 {
            padding-bottom: 0;
          }
        }
        .section__inside {
          right: 15%;
          width: 600px;
        }
    }
    
    @media(min-width: 1600px) {
        .section__inside {
          top: 18em;
        }
    }
`;

const Success = styled.p`
  color: lightgreen;
  font-weight: bold;
  margin-top: 2em;
`;

const ContactSection = (props) => {
    const [showSuccess, setShowSuccess] = useState(false);
    const onHandleSubmit = (value) => {
        props.addContact(value);
        setShowSuccess(true);
        setTimeout(() => {
            setShowSuccess(false);
        }, 4000)
    };

    return(
        <Footer id="contact">
            <div className="section__inside">
                <div className="section__title">
                    <h1>contact</h1>
                    <p>Canada Island, Division No. 23, Unorganized, MB, Canada</p>
                    <p>Tel. +1(778) 288 5180</p>
                </div>
                <ContactSectionForm onSubmit={onHandleSubmit} />
                <ReactCSSTransitionGroup
                    transitionName="fade"
                    transitionAppear={true}
                    transitionAppearTimeout={800}
                    transitionEnterTimeout={800}
                    transitionLeaveTimeout={800}
                >
                    {showSuccess && <Success>Message has been sent</Success>}
                </ReactCSSTransitionGroup>
            </div>
            <div id="section__img__wrap">
                <img src={contact} alt="contact-bg"/>
            </div>
        </Footer>
    );
}

const mapDispatchToProps = (dispatch) => ({
   addContact: (contact) => dispatch(addContact(contact))
});

export default connect(null, mapDispatchToProps)(ContactSection);