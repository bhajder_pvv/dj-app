import React from "react";
import styled from "styled-components";

const List = styled.ul`
       display: flex;
       ${props => props.isMobile ? "flex-direction:column;justify-content:space-between;height:60%;" : "flex-direction:row;justify-content:center;height:100%;"};
       align-items: center;
    }
`;

const Link = styled.li`

     position: relative;
     font-family: 'dosisregular';
     text-transform: uppercase;
     opacity: ${props => props.active ? "1" : ".5"};
     margin: 0 35px;
     transition: opacity .2s ease;
     
    :hover {
        opacity: 1;
        transition: opacity .2s ease;
    }
    
    ::after {
        position: absolute;
        display: block;
        width: 100%;
        height: 0;
        margin-top: 3px;
        background: #000;
        content:'';
        transition: height .2s ease;
    }
    
    :hover:after {
        height: 3px;
        transition: height .2s ease;
    }
    
    ${props => props.active && "::after{height: 3px;"}
`;

const links = [
    'about',
    'discography',
    'concert tours',
    'latter compositions',
    'new tracks',
    'upcoming events',
    'history',
    'contact'
];

const NavList = (props) => {

    return (
        <List isMobile={props.isMobile}>
            {links.map(link =>
                <Link onClick={props.handler} key={link}><a href={"#" + link.replace(" ", "")}>{link}</a></Link>
            )}
        </List>
    )
};

export default NavList;