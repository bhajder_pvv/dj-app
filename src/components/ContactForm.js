import React, { useState, useEffect } from 'react';
import { Field, reduxForm, reset, initialize } from 'redux-form';
import Input from './FormComponents/Input';
import styled from "styled-components";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebookF, faInstagram, faTwitter} from "@fortawesome/free-brands-svg-icons";
import { required, email, maxLength, date, id } from "../validators/validators";
import Datepicker from "./FormComponents/Datepicker";
import uuid from 'uuid';

const FormWrapper = styled.div`
    width: 100%;
    height: 16em;
    margin-top: 2em;
    position: relative;
    display: flex;
    justify-content: flex-start;
    
    .contact__social {
      position: relative;
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      margin: 1.5em 0 0 4em;
      align-items: center;
      height: 100%;
      width: auto;
      ::before {
        position: absolute;
        content: "";
        background: #000;
        width: 1px;
        height: 100%;
        top: 0;
        left: -2em;
      }
      
      .icon {
        transition: color .12s ease;
        :hover {
          cursor: pointer;
          transition: color .12s ease;
        }
      }
      
      .icon__fb:hover {
        color: #4267b2 !important;
      }
      
      .icon__ig:hover {
         color: #c13584 !important;
      }
      
      .icon__tw:hover {
        color: #38A1F3 !important;
      }
    }
    
    @media(min-width: 640px) {
      .content__social {
        margin-left: 8em;
      }
    }
`;

const FieldsWrapper = styled.div`
  width: 100%;
  height: 100%;
  .react-datepicker__navigation {
    background: none;
    line-height: 1.7rem;
    text-align: center;
    cursor: pointer;
    position: absolute;
    top: 10px;
    width: 0;
    padding: 0;
    z-index: 1;
    height: 10px;
    width: 10px;
    text-indent: -999em;
    overflow: hidden;
  }
`;

const Form = styled.form`
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
    input {
      display: block;
      border: solid #000;
      border-width: 0 0 1px 0;
      height: 3em;
      width: 100%;
      background: none;
    }
    button {
      align-self: flex-start;
    }
    #id {
      display: none;
    }
  
  @media(min-width: 640px) {
    width: 80%;
  }
`;

const Button = styled.button`
    position: relative;
    border: none;
    left: 0;
    margin-top: 3em;
`;

const length300 = maxLength(300);

const afterSubmit = (result, dispatch) => {
    dispatch(reset('contact'));
    initData.id = uuid();
};

const initData = {
    "name": '',
    "email": '',
    "message": '',
    "date": new Date(),
    "id": uuid()
};

const ContactForm = (props) => {
    const [mode, setMode] = useState('section');


    const handleInitialize = () => {
        props.initialize(props.contactData ? props.contactData : initData);
    }

    useEffect(() => {
        handleInitialize()
    }, []);

    useEffect(() => {
         props.contactData ? setMode('edit') : setMode('section');
    }, [props.contactData])

    return (
        <Form onSubmit={props.handleSubmit}>
            <FormWrapper>
                <FieldsWrapper>
                    <Field name="name" type="text" placeholder="name" component={Input} validate={required} />
                    <Field name="email" type="email" placeholder="email" component={Input} validate={[required, email]} />
                    <Field name="message" type="test" placeholder="message" component={Input} validate={[required, length300]} />
                    <Field name="date" component={Datepicker} validate={date} />
                </FieldsWrapper>
                {
                    (mode == 'section') &&
                        <div className="contact__social">
                            <div className="icon icon__fb"><FontAwesomeIcon icon={faFacebookF} /></div>
                            <div className="icon icon__ig"><FontAwesomeIcon icon={faInstagram} /></div>
                            <div className="icon icon__tw"><FontAwesomeIcon icon={faTwitter} /></div>
                        </div>
                }
            </FormWrapper>
            <Button className={props.contactData && 'btn btn__simple'} type="submit" >send</Button>
        </Form>
    );
}

export default reduxForm({
    form: 'contact',
    onSubmitSuccess: afterSubmit
})(ContactForm);