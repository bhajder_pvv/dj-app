import React from 'react';
import styled from 'styled-components';
import TextTruncate from "react-text-truncate";

const Section = styled.section`

    h2 {
    margin: 2em 0;
   }

   h3 {
    color: #282c34;
   }
        
   span {
      color: #282c34 !important;
   }
     
   .section__inside {
      flex-direction: row;
      flex-wrap: wrap;
      justify-content: space-between;
   }
   .content__year {
      width: 100%;
      z-index: 2;
   }
   .content__description {
      width: 100%;
      z-index: 2;
   }
   
    @media(min-width: 1800px) {
      h3 {
        color: #fff;
      }
      span {
        color: #798285 !important;
      }
     }
`;

const News = styled.div`
     position: relative;
     background: transparent;
     height: 320px;
     width: 100%;
     margin-top: 2em;
     padding: 30px;
     box-sizing: border-box;
     overflow: hidden;
     transition: all .2s ease;
     
     :hover {
        cursor: pointer;
        transition: all .2s ease;
        img {
          opacity: .4;
        }
        .content__description { 
          span {
            color: #000 !important;
          }
        }
     }
     
     img  {
      position: absolute;
      width: 100%;
      height: 100%;
      object-fit: cover;
      opacity: .2;
      top: 0;
      left: 0;
      z-index: -1;
      transition: opacity .2s ease;
     }
     
     .number {
      font-family: 'open_sansbold';
      font-weight: bold;
      position: absolute;
      bottom: -0.4em;
      right: .2em;
      font-size: 5em;
      opacity: .2;
     }
     
     @media(min-width: 1024px) {
      height: 420px;
      width: 380px;
     }
`;

const newsData = [{
    id: 1,
    title: 'James Bay - Let It Go (Bearson Remix)',
    description: 'A soulful singer/songwriter from a small English town with a penchant for crafting moving and evocative folk-pop confections in the vein of Ed Sheeran, Foy Vance, and Ben Howard, James Bay hails from the North Hertf shire market town of Hitchin. Bay honed his skills regionally, eventually landing ',
    name: 'chill nation',
    date: '03.04.2015'
}, {
    id: 2,
    title: 'Smoke and Mirrors',
    description: 'Hard Rock Cafe teamed with the band, granting them the first ever full access to take control of Hard Rock Cafe\'s internal video system (more than 20,000 screens at all 151 locations ',
    name: 'chill nation',
    date: '01.03.2015'
}, {
    id: 3,
    title: 'It Comes Back to You',
    description: 'In 2015 a world tour, entitled the Smoke and Mirrors Tour is scheduled in promotion of the world-wide release of Smoke and Mirrors.',
    name: 'chill nation',
    date: '04.04.2015'
}, {
    id: 4,
    title: 'Let It Go (Bearson Remix)',
    description: 'A soulful singer/songwriter from a small English town with a penchant for crafting moving and evocative folk-pop confections in the vein of Ed Sheeran, Foy Vance, and Ben ',
    name: 'chill nation',
    date: '01.07.2015'
}, {
    id: 5,
    title: 'I Don\'t Mind',
    description: 'Hell and Silence is an EP by Las Vegas rock group Imagine Dragons, released in March 2010 in the United States. It was recorded at Battle Born Studios.[1] All songs were written by Imagine Dragons and self-produced. The EP was in part mixed by Grammy nominated engineer Mark Needham.',
    name: 'chill nation',
    date: '02.11.2015'
}, {
    id: 6,
    title: 'Smoke + Mirrors',
    description: 'A soulful singer/songwriter from a small English town with a penchant for crafting moving and evocative folk-pop confections',
    name: 'chill nation',
    date: '06.09.2015'
}];

const NewsSection = () => {
    return (
        <Section id="newtracks">
            <div className="section__inside">
                {newsData.map((news, index) => <News key={news.id}>
                    <div className="content__year">
                        <h4>{news.date}</h4>
                        <hr />
                        <h3>{news.name}</h3>
                    </div>
                    <div className="content__description">
                        <h2>{news.title}</h2>
                        <TextTruncate line={4} truncateText="..." text={news.description}/>
                    </div>
                    <div className="number">0{index+1}</div>
                    <img src={require("../Assets/images/news/" + news.id + ".jpg")} alt="bg"/>
                </News>)}
            </div>
        </Section>
    )
};

export default NewsSection;