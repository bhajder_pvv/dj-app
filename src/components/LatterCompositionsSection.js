import React from 'react';
import styled from 'styled-components';
import LatterCompositionsList from './LatterCompositionsList';

const Section = styled.section`
    #compositions {
      width: 100%;
    }
`;

const LatterCompositionsSection = () => {
    return (
        <Section id="lattercompositions">
            <div className="section__inside">
            <div className="section__title">
                <h1>
                latter compositions
                </h1>
            </div>
            <p className="section__subtitle">
                "It's Time" was released as the lead single from Continued Silence and It's Time, both
                extended plays preceding Night Visions' release.
            </p>
            <div id="compositions">
                <LatterCompositionsList />
            </div>
            </div>
        </Section>
    );
}

export default LatterCompositionsSection;